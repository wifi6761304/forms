<?php
namespace FormLib;

class Button extends Input {
	public function __construct(array $config) {
		parent::__construct($config);
		// type can be: button (for javascript), submit (default), reset
		$this->type = $config['type'] ?? 'submit';
		// bootstrap default class
		$this->class = $config['class'] ?? 'btn btn-primary';
	}

	public function render(): string
	{
		$out = '';
		$out .= <<<FIELD
		<div class="mb-3">
			{$this->renderField()}
		</div>
FIELD;
		return $out;
	}

	/**
	 * Render button field
	 *
	 * @return string
	 */
	public function renderField(): string
	{
		$out = '';
		$out .= <<<BTN
		<button type="$this->type" id="$this->id" name="$this->name"{$this->renderTagAttributes()}>$this->label</button>
BTN;
		return $out;
	}
}