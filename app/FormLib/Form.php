<?php

namespace FormLib;

/**
 * Manager Class for Forms
 * - creates a new Form
 * - manages configuration
 * - creates Inputs from configuration
 * - validates Inputs
 * - renders Form
 * 		- renders all Inputs
 *
 * depends on bootstrap
 */
class Form
{
	private array $config = [];
	private array $fields = [];
	private array $validData = [];

	// config attributes
	private string $action;
	private string $method;
	private string $enctype;
	private array $tagAttributes;

	public function __construct(array $config)
	{
		$this->config = $config;
		// Zuweisung aus $config, ggf. default Werte zuweisen, falls diese
		// nicht übergeben wurden
		$this->action = $config['action'] ?? '';
		$tempMethod = $config['method'] ?? 'post';
		// method darf nur get oder post sein
		// if ($tempMethod !== 'get' && $tempMethod !== 'post')
		if (!in_array($tempMethod, ['post', 'get'])) {
			$tempMethod = 'post';
		}
		$this->method = $tempMethod;
		$this->enctype = $config['enctype'] ?? '';

		// tagAttributes müssen ein Array sein
		$this->tagAttributes = $config['tagAttributes'] ?? [];
		if(!is_array($this->tagAttributes)) {
			$this->tagAttributes = [];
		}

		// create form fields
		$fields = $config['fields'] ?? [];
		if(is_array($config)) {
			$this->createFields($fields);
		}
	}

	/**
	 * Render form and its fields
	 *
	 * @return string
	 */
	public function render(): string
	{
		$out = '';
		$out .= $this->renderFormOpen();
		// Ausgabe der Inputs
		$out .= $this->renderFields();
		$out .= $this->renderFormClose();
		return $out;
	}

	/**
	 * Render form open tag
	 * @return string
	 */
	public function renderFormOpen(): string
	{
		return <<<FORM
	<form action="$this->action" method="$this->method" enctype="$this->enctype"{$this->renderTagAttributes()}>
FORM;
	}

	/**
	 * Render closing tag
	 *
	 * @return string
	 */
	public function renderFormClose(): string
	{
		return '</form>';
	}

	/**
	 * Render all fields (inputs, selects, textareas, etc)
	 *
	 * @return string
	 */
	public function renderFields(): string
	{
		$out = '';
		foreach($this->fields as $field) {
			$out .= $field->render();
		}
		return $out;
	}

	public function validate(): bool
	{
		// Aus den Inputs die Validierungsregeln auslesen
		$rules = $this->getValidationRules();
		// GUMP Validator initialisieren
		$gump = new \GUMP('de');
		$gump->validation_rules($rules);

		// GUMP Validator ausführen
		$data = [];
		if ($this->method === 'get') {
			$data = $_GET;
		} else {
			$data = $_POST;
		}

		$validData = $gump->run($data);

		if($gump->errors()) {
			$errors = $gump->get_errors_array();
			$this->setErrors($errors);
			// Daten in $this->fields schreiben
			$this->prefillFields($data);
			return false;
		}
		// Daten sind OK!
		// validerte Daten speichern und auslesbar machen
		$this->validData = $validData;
		// Ergebnis zurückgeben
		return true;
	}

	/**
	 * Get valid data after validation
	 *
	 * @return array
	 */
	public function getValidData(): array
	{
		return $this->validData;
	}

	/**
	 * Check if form was sent
	 *
	 * @return boolean
	 */
	public function isSent(): bool
	{
		/*
			Ternary operator: if / else in one line
			condition ? true : false
			return $this->method === 'get' ? !empty($_GET) : !empty($_POST);
		*/
		if ($this->method === 'get')	{
			return !empty($_GET);
		}
		return !empty($_POST);
	}

	/**
	 * Set errors for each field that has an error
	 *
	 * @param array $errors
	 * @return void
	 */
	private function setErrors(array $errors)
	{
		foreach($errors as $name => $error) {
			$this->fields[$name]->setError($error);
		}
	}

	/**
	 * Get validation rules from all fields
	 *
	 * @return array
	 */
	private function getValidationRules(): array
	{
		$rules = [];
		foreach($this->fields as $name => $field) {
			$rule = $field->getValidationRules();
			if($rule !== null) {
				$rules[$name] = $rule;
			}
		}
		return $rules;
	}

	/**
	 * Prefill fields with data
	 *
	 * @param array $data
	 * @return void
	 */
	private function prefillFields(array $data)
	{
		foreach($data as $name => $value) {
			$this->fields[$name]->prefill($value);
		}
	}

	/**
	 * Convert tag attributes to valid html attributes
	 *
	 * @return string
	 */
	private function renderTagAttributes(): string
	{
		$out = '';

		foreach($this->tagAttributes as $key => $value) {
			$out .= " $key=\"$value\"";
		}

		return $out;
	}

	/**
	 * Create Input Object for each type. Store it in $this->fields.
	 *
	 * @param array $fields
	 * @return void
	 */
	private function createFields(array $fields)
	{
		foreach($fields as $name => $fieldConfig) {
			// Wir brauchen keinen namespace, da beide Klassen sich im selben
			// namespace befinden.
			switch($fieldConfig['type']){
				case 'textarea':
					$this->fields[$name] = new Textarea($fieldConfig);
					break;
				case 'select':
					$this->fields[$name] = new Select($fieldConfig);
					break;
				// ohne break wird der case 'submit' auch für case 'button' und case 'reset' ausgeführt
				case 'submit':
				case 'button':
				case 'reset':
					$this->fields[$name] = new Button($fieldConfig);
					break;
				default:
					$this->fields[$name] = new Input($fieldConfig);
			}
		}
	}
}
