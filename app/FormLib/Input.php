<?php

namespace FormLib;

class Input
{
	private array $config;

	protected string $type;
	protected string $name;
	protected string $id;
	protected string $label;
	protected string $class;
	protected string $value;
	protected string $error = '';
	// Modifikator ?: bedeutet, dass der Wert auch null sein kann.
	protected ?string $validation;
	protected array $tagAttributes;

	public function __construct(array $config)
	{
		$this->config = $config;
		$this->type = $config['type'] ?? 'text';
		$this->name = $config['name'];
		$this->label = $config['label'] ?? $this->name;
		$this->id = $config['id'] ?? $this->name;
		$this->value = $config['value'] ?? '';
		$this->class = $config['class'] ?? 'form-control';
		$this->validation = $config['validation'] ?? null;

		$this->tagAttributes = $config['tagAttributes'] ?? [];
		if (!is_array($this->tagAttributes)) {
			$this->tagAttributes = [];
		}

		/*
			Wenn in class eine Klasse steht, wollen wir diese in tagAttributes[class] anfügen.
			Wenn tagAttributes[class] nicht exisitiert, weisen wir class einfach zu.
		*/
		if (!empty($this->class)) {
			if (array_key_exists('class', $this->tagAttributes)) {
				$this->tagAttributes['class'] .= " {$this->class}";
			} else {
				$this->tagAttributes['class'] = $this->class;
			}
		}
	}

	public function render(): string
	{
		$out = '';
		$out .= <<<FIELD
		<div class="mb-3">
			{$this->renderLabel()}
			{$this->renderField()}
			{$this->renderError()}
		</div>
FIELD;
		return $out;
	}

	public function renderLabel(): string
	{
		$out = '';
		$out .= <<<LABEL
		<label for="$this->id" class="form-label">$this->label</label>
LABEL;
		return $out;
	}

	public function renderField(): string
	{
		$out = '';
		$out .= <<<FIELD
		<input type="$this->type" id="$this->id" value="$this->value" name="$this->name"{$this->renderTagAttributes()}>
FIELD;
		return $out;
	}

	/**
	 * Render error message
	 *
	 * @return string
	 */
	public function renderError(): string
	{
		$out = '';
		if (!empty($this->error)) {
			$out .= <<<ERR
			<div class="text-danger">
				$this->error
			</div>
ERR;
		}
		return $out;
	}

	public function setError(string $error)
	{
		$this->error = $error;
	}

	/**
	 * Prefill field with value, mostly used after validation
	 *
	 * @param string $value
	 * @return void
	 */
	public function prefill(string $value)
	{
		$this->value = $value;
	}

	/**
	 * Get validation rules
	 *
	 * @return string|null
	 */
	public function getValidationRules(): ?string
	{
		return $this->validation;
	}

	/**
	 * Convert tag attributes to valid html attributes
	 *
	 * @return string
	 */
	protected function renderTagAttributes(): string
	{
		$out = '';

		foreach ($this->tagAttributes as $key => $value) {
			$out .= " $key=\"$value\"";
		}

		return $out;
	}
}
