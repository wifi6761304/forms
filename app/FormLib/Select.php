<?php
namespace FormLib;

class Select extends Input {
	private array $options;

	public function __construct(array $config) {
		parent::__construct($config);
		$this->options = $config['options'] ?? [];
	}

	/**
	 * Render select field
	 *
	 * @return string
	 */
	public function renderField(): string
	{
		$out = '';
		$out .= <<<SEL
		<select id="$this->id" name="$this->name"{$this->renderTagAttributes()}>
		{$this->renderOptions()}
		</select>
SEL;
		return $out;
	}

	/**
	 * Render select options
	 *
	 * @return string
	 */
	private function renderOptions(): string
	{
		$out = '';
		foreach($this->options as $value => $text) {
			$out .= <<<OPT
			<option value="$value">$text</option>
OPT;
		}
		return $out;
	}
}