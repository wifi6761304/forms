<?php
namespace FormLib;

class Textarea extends Input {
	public function renderField(): string
	{
		$out = '';
		$out .= <<<TA
	<textarea id="$this->id" name="$this->name"{$this->renderTagAttributes()}>$this->value</textarea>
TA;
		return $out;
	}
}