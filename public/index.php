<?php
require_once "../init.php";
?><!DOCTYPE html>
<html lang="de">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Unsere eigene Forms Library</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<header>
			<h1>Unsere eigene Forms Library</h1>
		</header>
		<main>
			<?php
			// TODO: Fallbacks wenn Datei nicht geladen werden kann (try catch)
			$configJson = file_get_contents(APP_ROOT . '/formConfig/contactForm.json');
			// konvertieren in ein PHP Array
			$formConfig = json_decode($configJson, true);

			$form = new FormLib\Form($formConfig);
			if($form->isSent()){
				$isValid = $form->validate();
			}
			echo $form->render();
			?>
		</main>
	</div>
</body>
</html>